#ifndef NUMBER_PARTITION_HPP
#define NUMBER_PARTITION_HPP

#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iterator>
#include <boost/heap/fibonacci_heap.hpp>
#include <cstdlib>
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>


namespace detail {
  struct heap_node {
      explicit heap_node(int apart = 0, int asum = 0) : part(apart), sum(asum) { }

      int part;
      int sum;
  }; // heap_node

  inline bool operator<(const heap_node& lhs, const heap_node& rhs) {
      return rhs.sum < lhs.sum;
  } // operator<
} // namespace detail


int sumVector (std::vector <int> S, std::vector<bool> s) {
        int sum = 0;
        for (unsigned int i=0; i< S.size(); i++) {
                if (s[i] == true)
                        sum += S[i];
        }
        return sum;
}


int greedy_npartition(const std::vector<int>& S, int k, std::vector<int>& P) {
    int n = S.size();

    // create index representing sorted S
    std::vector<int> index(n);

    std::iota(index.begin(), index.end(), 0);
    std::sort(index.begin(), index.end(), [&S](int x, int y) { return S[x] > S[y]; });

    P.resize(n);

    typedef boost::heap::fibonacci_heap<detail::heap_node> heap_type;
    std::vector<heap_type::handle_type> handle(k);
    heap_type H;

    int lim = std::min(k, n);

    // initialize partitions heap
    for (int i = 0; i < lim; ++i) {
        P[index[i]] = i;
        handle[i] = H.push(detail::heap_node(i, S[index[i]]));
      }

    // update heap
    for (int i = lim; i < n; ++i) {
        detail::heap_node hn = H.top();
        P[index[i]] = hn.part;
     	   
	hn.sum += S[index[i]];
        H.update(handle[hn.part], hn);
    }

    for (int i = 0; i < k-1; i++)
    	H.pop();
    return H.top().sum;	
} // greedy_npartition

// Finds each set recursively
int* partitionP(std::vector<int>& A, std::vector<bool>& a, int k, int low,int high, int& cmax, int& cmin) {
	if (std::accumulate(A.begin(), A.end(), 0) <= cmin) {
		int *ret = (int*)malloc((A.size()+1)*sizeof(int));
		*ret = A.size();
		for (int i = 0; i < A.size(); i++) {
			*(ret+i+1) = A[i];
		}
		return ret;
	}

	if (low>high) {
		std::vector <int> temp;
		if (cmax >= sumVector(A, a) && cmin <= sumVector(A, a))
		for(unsigned int i = 0; i < A.size(); i++) {
			if (a[i] == true)
				temp.push_back(A[i]);
		}
		int *ret = (int*)malloc((temp.size()+1)*sizeof(int));
		*ret = temp.size();
		for (int i = 0; i < temp.size(); i++) {
			*(ret+i+1) = temp[i];
		}
		return ret;
	}
	else {
		a[low] = true;
		if (cmax >= sumVector(A, a)) {
			int* retval;
			retval = cilk_spawn partitionP(A, a, k, low+1, high, cmax, cmin);
			//retval = partitionP(A, a, k, low+1, high, cmax, cmin);
			return retval;		
		}  

		a[low] = false;
		if (cmax >= sumVector(A, a)) {
			std::vector <int> retval;
			return partitionP(A, a, k, low+1, high, cmax, cmin); 
		}  
	}
}

void moffitt_npartition(const std::vector<int>& S, int k, int cmin, int cmax, std::vector<int>& P) {
	std::vector<int> A(S);
	std::vector <bool> a (A.size(), false);
	
	int set_num = 0;
	
	while (set_num < k && A.size() > 0) {
		std::vector <int> sub_s;		
		int* ret = partitionP(A, a, k, 0, A.size()-1, cmax, cmin);
		for (int i = 0; i< *ret; i++) {
			 sub_s.push_back(*(ret+i+1));
		}
		// Assigning elements
		for (unsigned int i = 0; i < sub_s.size(); i++) {
			auto it_S_subs = std::find(S.begin(), S.end(), sub_s[i]);
			auto indexS = std::distance(S.begin(), it_S_subs);

			while (P[indexS] != -1 && it_S_subs!=S.end()) {
				it_S_subs = std::find(it_S_subs+1, S.end(), sub_s[i]);
				indexS = std::distance(S.begin(), it_S_subs);
			}

			auto it_A_subs = std::find(A.begin(), A.end(), sub_s[i]);
			auto indexA = std::distance(A.begin(), it_A_subs);

			if (P[indexS]==-1) {
				P[indexS] = set_num;
				A[indexA] = 0;
			}
		}
		
		A.erase(std::remove(A.begin(), A.end(), 0), A.end());

		cmax = std::accumulate(sub_s.begin(), sub_s.end(), 0);
		cmin = *std::max_element(sub_s.begin(), sub_s.end());

		set_num++;
		a.resize(A.size());
		std::fill(a.begin(), a.end(), false);
	}
	std::vector<int>::iterator it;
	while (it!=P.end()) if ((it = find (P.begin(), P.end(), -1)) != P.end()) *it = k-1;
} // moffitt_npartition

// Implements k-way partitioning of S, P[i] stores partition assignment for S[i]
void moffitt_npartition(const std::vector<int>& S, int k, std::vector<int>& P) {
    int cmin = *std::max_element(S.begin(), S.end());
    int cmax = std::accumulate(S.begin(), S.end(), 0);
    
    moffitt_npartition(S, k, cmin, cmax, P);
} // moffitt_npartition

#endif // NUMBER_PARTITION_HPP

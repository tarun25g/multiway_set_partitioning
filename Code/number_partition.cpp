#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>
#include <iostream>
#include <algorithm>
#include <numeric>
#include <vector>
#include <iterator>
#include <boost/heap/fibonacci_heap.hpp>
#include <cstdlib>
#include <time.h> 
#include "number_partition.hpp"

int verification (std::vector<int>& A, std::vector<int>& P, int k);
int main() {
	std::vector <int> S;
	int k =5;		// number of partitions

	std::vector <int> A;
	srand (time(NULL));
	int num_elem = 4775;				// number of elements in set S
	int range_elem = 10;				// range of random Numbers used as elements in set
	for(int i = 0; i < num_elem; i++) {
		A.push_back(rand()%range_elem + 1);
	}

	std::vector <int> P(A.size(), -1);

	std::vector <bool> a (A.size(), false);
	int d = 0;
	int cmax = greedy_npartition(A, k, P);
	int cmin = 0;
	
	int set_num = 1;

	//std::cout << "\nRunning Greedy Partitioning\n";
	std::cout << "\ncmin is "<< cmin << " and cmax is " << cmax << "\n";
	std::cout << "\nTEST PRINT:: P after greedy partition is-\n";
	for (int i = 0; i < P.size(); i++) {
		std::cout << P[i] << "\t";
	}		
	std::cout << "\n\n";
	
	std::fill(P.begin(), P.end(), -1);

	std::sort(A.begin(), A.end(), std::greater<int>());
	
	/*
	std::cout << "\nTEST PRINT:: A is -\n";
	for (int i = 0; i < A.size(); i++) {
		std::cout << A[i] << "\t";
	}		
	std::cout << "\n\n";
	*/
	clock_t t = clock();
	moffitt_npartition(A, k, cmin, cmax, P);
	t = clock() - t;



	std::cout << "\nPRINT:: P is now-\n";
	for (int i = 0; i < P.size(); i++) {
		std::cout << P[i] << "\t";
	}		
	std::cout << "\n\n";

	printf ("It took %f seconds.\n",((float)t)/CLOCKS_PER_SEC);
	
	verification (A, P, k);
}


/* Verification Module */
int verification (std::vector<int>& A, std::vector<int>& P, int k) {
	int sum[k];
	for (int i = 0; i < k; i++) sum[i] = 0;

	for (int i  = 0; i < A.size(); i++) {
		sum[P[i]] = sum[P[i]] + A[i];
	}

	std::cout << "\n\nPinting all sum--\n";
	for (int i = 0; i < k; i++) std::cout << sum[i] << "\t";
	std::cout << "\n***\n";
}



